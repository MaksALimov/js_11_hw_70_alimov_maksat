import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faCartPlus} from "@fortawesome/free-solid-svg-icons";
import {getProducts} from "../../store/actions/productsActions";
import Cart from "../Cart/Cart";
import './Products.css';
import {cartProducts} from "../../store/actions/cartActions";

const Products = () => {
    const dispatch = useDispatch();
    const products = useSelector(state => state.products.products);
    const cartArray = useSelector(state => state.cart.cartItem);

    useEffect(() => {
        dispatch(getProducts())
    }, [dispatch]);
    return (
        <div className="ProductsWrapper">
            <div className="ProductsWrapper__inner">
                {products.map(product => (
                    <div key={product.name} className="ProductsWrapper__content">
                        <img src={product.image} alt="product"/>
                        <div className="ProductsWrapper__info">
                            <h3 className="ProductsWrapper__title">{product.name}</h3>
                            <p className="ProductsWrapper__price">KGS: {product.price}</p>
                            <button className="ProductsWrapper__add-cart" onClick={() => dispatch(cartProducts(product, cartArray))}>
                                <FontAwesomeIcon icon={faCartPlus}/>
                                <span>Add to cart</span>
                            </button>
                        </div>
                    </div>
                ))}
            </div>
            <Cart/>
        </div>
    );
};

export default Products;
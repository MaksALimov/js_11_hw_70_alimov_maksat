import React from 'react';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faTrashAlt} from "@fortawesome/free-solid-svg-icons";
import {useDispatch, useSelector} from "react-redux";
import {deleteCartProduct, showModal} from "../../store/actions/cartActions";
import './Cart.css';
import Modal from "../Modal/Modal";
import Backdrop from "../Backdrop/Backdrop";

const Cart = () => {
    const dispatch = useDispatch();
    const cartProduct = useSelector(state => state.cart.cartItem);
    const totalPrice = useSelector(state => state.cart.totalPrice);
    const delivery = useSelector(state => state.cart.delivery);
    const modal = useSelector(state => state.cart.modal);
    return (
        <fieldset>
            <legend>Cart</legend>
            {cartProduct.map(product => (
                <div key={product.name} className="CartProducts">
                    <div>
                        <p>{product.name}: x{product.amount}</p>
                        <p>Стоимость: {product.price * product.amount}</p>
                    </div>
                    <button className="DeleteCartBtn"
                            onClick={() => dispatch(deleteCartProduct(product.name, product.price, product.amount))}>
                        <FontAwesomeIcon icon={faTrashAlt}/>
                        <span className="DeleteCartProduct">Delete</span>
                    </button>
                </div>
            ))}
            <p className="delivery"> {cartProduct.length > 0 ? <span>Доставка: {delivery}</span> : null}</p>
            <p className="totalPrice">{cartProduct.length > 0 ? <span>Итого: {totalPrice}</span> : null} </p>
            {cartProduct.length > 0 ? <button className="PlaceOrder" onClick={() => dispatch(showModal())}>Place order</button>: null}
            {modal ? <> <Modal/><Backdrop/> </> : null}
        </fieldset>
    );
};

export default Cart;
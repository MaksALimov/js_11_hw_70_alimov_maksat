import React from 'react';
import './Backdrop.css';

const Backdrop = () => {
    return <div className="Backdrop"/>;
};

export default Backdrop;
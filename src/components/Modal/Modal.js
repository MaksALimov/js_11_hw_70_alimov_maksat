import React, {useState} from 'react';
import './Modal.css';
import {useDispatch} from "react-redux";
import {
    addProduct,
    showModal
} from "../../store/actions/cartActions";

const Modal = () => {
    const dispatch = useDispatch();
    const [user, setUser] = useState({
        name: null,
        tel: null,
        street: null,
    })

    const createOrder = e => {
        e.preventDefault();
    };

    const onInputChange = e => {
        e.preventDefault();
        const {name, value} = e.target;
        setUser(prevState => ({...prevState, [name] : value}))
    };

    return (
        <form className="ModalWrapper" onSubmit={createOrder}>
            <input
                type="text"
                name="name"
                placeholder="Введите своё имя"
                onChange={onInputChange}
            />
            <input
                type="text"
                name="street"
                placeholder="Улица"
                onChange={onInputChange}
            />
            <input
                type="tel"
                name="tel"
                placeholder="Введите свой номер"
                onChange={onInputChange}
            />
            <button onClick={() => dispatch(addProduct(user))}>Создать заказ</button>
            <button onClick={() => dispatch(showModal())}>Закрыть</button>
        </form>
    );
};

export default Modal;
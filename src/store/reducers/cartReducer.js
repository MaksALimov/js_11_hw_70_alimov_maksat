import {
    CART_FAILURE,
    CART_PRODUCTS,
    CART_REQUEST,
    CART_SUCCESS,
    DELETE_CART_PRODUCT,
    SHOW_MODAL
} from "../actions/cartActions";

const initialState = {
    cartItem: [],
    delivery: 150,
    totalPrice: 0,
    modal: false,
    error: null,
};

const cartReducer = (state = initialState, action) => {
    switch (action.type) {
        case CART_PRODUCTS: {
            return {...state, cartItem: action.payload, totalPrice: state.totalPrice + action.price + state.delivery};
        }

        case DELETE_CART_PRODUCT: {
            return {
                ...state, cartItem: state.cartItem.filter(cart => cart.name !== action.payload),
                totalPrice:  (state.totalPrice - action.price * action.amount) - (state.delivery * action.amount)
            };
        }

        case SHOW_MODAL: {
            return {...state, modal: !state.modal}
        }

        case CART_REQUEST: {
            return {...state};
        }

        case CART_SUCCESS: {
            return {...state, error: false};
        }

        case CART_FAILURE: {
            return {...state, error: action.payload};
        }

        default:
            return state;
    }
};

export default cartReducer;
import {
    PRODUCTS_FAILURE,
    PRODUCTS_REQUEST,
    PRODUCTS_SUCCESS
} from "../actions/productsActions";

const initialState = {
    products: [],
    error: null,
};

const productsReducer = (state = initialState, action) => {
    switch (action.type) {
        case PRODUCTS_REQUEST: {
            return {...state};
        }

        case PRODUCTS_SUCCESS: {
            return {...state, products: action.payload};
        }

        case PRODUCTS_FAILURE: {
            return {...state, error: action.payload};
        }

        default:
            return state;
    }
};

export default productsReducer;
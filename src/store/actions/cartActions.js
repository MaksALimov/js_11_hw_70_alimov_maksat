import axiosApi from "../../axiosApi";

export const CART_PRODUCTS = 'CART_PRODUCTS';
export const DELETE_CART_PRODUCT = 'DELETE_CART_PRODUCT';
export const SHOW_MODAL = 'SHOW_MODAL';

export const CART_REQUEST = 'CART_REQUEST';
export const CART_SUCCESS = 'CART_SUCCESS';
export const CART_FAILURE = 'CART_FAILURE';

export const cartProduct = (cartProduct, price) => ({type: CART_PRODUCTS, payload: cartProduct, price});
export const deleteCartProduct = (name, price, amount) => ({type: DELETE_CART_PRODUCT, payload: name, price: price, amount: amount});
export const showModal = () => ({type: SHOW_MODAL});

export const cartRequest = () => ({type: CART_REQUEST});
export const cartSuccess = () => ({type: CART_SUCCESS});
export const cartFailure = error => ({type: CART_FAILURE, payload: error});

export const cartProducts = (cartProductItem, cartArray) => {
    return dispatch => {
        let cartCopy = [...cartArray];
        const index = cartCopy.findIndex(id => id.name === cartProductItem.name);
        if (index === -1) {
            cartCopy.push({name: cartProductItem.name, price: cartProductItem.price, amount: 1});
        } else {
            const amountedItem = cartCopy.map(item => {
                if (item.name === cartProductItem.name) {
                    item.amount++;
                }
                return item;
            });
            return dispatch(cartProduct(amountedItem, cartProductItem.price));
        }

        dispatch(cartProduct(cartCopy, cartProductItem.price));
    };
};

export const addProduct = (user) => {
    return async (dispatch, getState) => {

        const products =  getState().cart.cartItem;
        const cartProducts = {};

        for (let n = 0; n < products.length; n++) {
            cartProducts[products[n]['name']] = products[n]['amount'];
        }

        try {
            dispatch(cartRequest());
            await axiosApi.post('/users.json', {user, cartProducts});
            dispatch(cartSuccess());
        } catch (error) {
            dispatch(cartFailure(error));
        }
    };
};

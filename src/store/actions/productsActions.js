import axiosApi from "../../axiosApi";

export const PRODUCTS_REQUEST = 'PRODUCTS_REQUEST';
export const PRODUCTS_SUCCESS = 'PRODUCTS_SUCCESS';
export const PRODUCTS_FAILURE = 'PRODUCTS_SUCCESS';

export const productsRequest = () => ({type: PRODUCTS_REQUEST});
export const productsSuccess = products => ({type: PRODUCTS_SUCCESS, payload: products});
export const productsFailure = error => ({type: PRODUCTS_FAILURE, payload: error});

export const getProducts = () => {
    return async dispatch => {
        try {
            dispatch(productsRequest());

            const response = await axiosApi('/products.json');
            const products = Object.values(response.data);
            dispatch(productsSuccess(products));

        } catch (error) {
            dispatch(productsFailure(error));
        }
    };
};

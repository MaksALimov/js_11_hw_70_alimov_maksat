import axios from "axios";

const axiosApi = axios.create({
    baseURL: 'https://restaurant-menu-js-11-default-rtdb.firebaseio.com'
});

export default axiosApi;